class Visit
  attr_reader :log_entry

  def initialize(log_entry)
    @log_entry = log_entry
  end

  def ==(other)
    log_entry == other.log_entry
  end

  def webpage
    parsed_log[0]
  end

  def ip_address
    parsed_log[1]
  end

  private

  def parsed_log
    log_entry.split("\s")
  end
end
