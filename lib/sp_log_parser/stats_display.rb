class StatsDisplay
  class << self
    def display(stats, unique: false)
      sorted_stats(stats).each do |stat|
        puts "#{stat.webpage} => #{pluralize(stat.views, unique)}"
      end
    end

    private

    def sorted_stats(stats)
      stats.sort_by(&:views).reverse
    end

    def pluralize(count, unique)
      word = count == 1 ? "view" : "views"

      [(count || 0), ("unique" if unique), word].compact.join(" ")
    end
  end
end
