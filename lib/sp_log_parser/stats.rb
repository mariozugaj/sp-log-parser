class Stats
  Stat = Struct.new(:webpage, :views)

  def initialize(visits)
    @visits = visits
  end

  def views
    visits.unique_webpages.map do |webpage|
      Stat.new(webpage, views_for(webpage).size)
    end
  end

  def unique_views
    visits.unique_webpages.map do |webpage|
      Stat.new(webpage, unique_views_for(webpage).size)
    end
  end

  private

  attr_reader :visits

  def views_for(webpage)
    visits.for(webpage)
  end

  def unique_views_for(webpage)
    views_for(webpage).uniq { |visit| visit.ip_address }
  end
end
