class Visits
  attr_reader :bucket

  def initialize
    @bucket = []
  end

  def <<(visit)
    @bucket << visit
  end

  def unique_webpages
    @unique_webpages ||= webpages.uniq
  end

  def for(webpage)
    @bucket.select { |visit| visit.webpage == webpage }
  end

  private

  def webpages
    @bucket.map(&:webpage)
  end
end
