class Parser
  def self.parse(logs)
    return if logs.empty?

    visits = Visits.new
    logs.inject(visits) do |visits_bucket, log_entry|
      visits_bucket << Visit.new(log_entry.strip)
    end
    visits
  end
end
