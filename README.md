# LogParser

Simple log parser for you webserver logs.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sp_log_parser'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install sp_log_parser

## Usage

Execute the following to parse your log file and display visit statistics:

```shell
$ sp_parser $PATH_TO_YOUR_LOG_FILE
```

You will get output similar to this:
```
--------Views--------------
/help_page/1 => 5 views
/home => 2 views
/contact => 2 views
/about => 1 view
/index => 1 view
/about/2 => 1 view
-----Unique Views----------
/help_page/1 => 5 unique views
/home => 2 unique views
/about => 1 unique view
/index => 1 unique view
/about/2 => 1 unique view
/contact => 1 unique view
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/mariozugaj/sp-log-parser.


## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
