require "sp_log_parser"

RSpec.describe Visit do
  let(:logs) { File.readlines("spec/support/webserver.log") }
  let(:visits) { Parser.parse(logs) }
  let(:stats) { Stats.new(visits) }

  describe "#views" do
    it "returns Stat object" do
      expect(stats.views.first.class).to eq Stats::Stat
    end

    it "returns correct results" do
      expected =
        [
          ["/help_page/1", 5],
          ["/contact", 2],
          ["/home", 2],
          ["/about/2", 1],
          ["/index", 1],
          ["/about", 1]
        ]
      actual = stats.views.map {|stat| [stat.webpage, stat.views] }
      expect(actual).to match_array expected
    end
  end

  describe "#unique_views" do
    it "returns Stat object" do
      expect(stats.unique_views.first.class).to eq Stats::Stat
    end

    it "returns correct results" do
      expected =
        [
          ["/help_page/1", 5],
          ["/contact", 1],
          ["/home", 2],
          ["/about/2", 1],
          ["/index", 1],
          ["/about", 1]
        ]
      actual = stats.unique_views.map {|stat| [stat.webpage, stat.views] }
      expect(actual).to match_array expected
    end
  end
end
