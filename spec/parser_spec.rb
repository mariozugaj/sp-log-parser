require "sp_log_parser"

RSpec.describe Parser, ".parse" do
  let(:logs) { File.readlines("spec/support/webserver.log") }
  let(:visits) { Parser.parse(logs) }

  context "when logs are empty" do
    let(:logs) { [] }

    it "returns nil" do
      expect(visits).to be_nil
    end
  end

  context "when logs are not empty" do
    it "returns visit objects" do
      expect(visits.bucket.first.class).to eq Visit
    end

    it "returns visits object" do
      expect(visits.class).to eq Visits
    end

    it "parses all logs" do
      expect(visits.bucket.size).to eq 12
    end
  end
end
