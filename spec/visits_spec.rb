require "sp_log_parser"

RSpec.describe Visits do
  let(:logs) { File.readlines("spec/support/webserver.log") }
  let(:visits) { Visits.new }

  it "is initialized with an empty bucket" do
    expect(visits.bucket).to be_empty
  end

  describe "#<<" do
    it "adds new visits to the bucket" do
      expect(visits.bucket.size).to eq 0

      visits << Visit.new(logs[0])
      visits << Visit.new(logs[1])
      visits << Visit.new(logs[2])

      expect(visits.bucket.size).to eq 3
    end
  end

  describe "#unique_webpages" do
    let(:visits) { Parser.parse(logs) }

    it "returns unique webpages from the bucket" do
      expected = [
        "/help_page/1",
        "/contact",
        "/home",
        "/about",
        "/index",
        "/about/2"
      ]

      expect(visits.unique_webpages).to match_array expected
    end
  end

  describe "#for" do
    let(:visits) { Parser.parse(logs) }

    it "returns specific visit for a given webpage" do
      expected = Visit.new("/index 444.701.448.104")

      expect(visits.for("/index").first).to eq expected
    end
  end
end
