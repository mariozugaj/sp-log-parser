require "sp_log_parser"

RSpec.describe Visit do
  let(:visit) { Visit.new("/index 444.701.448.104") }

  describe "#==" do
    it "compares two visits based on the log_entry input" do
      expect(visit).to eq Visit.new("/index 444.701.448.104")
    end
  end

  describe "#webpage" do
    it "returns visit\'s webpage" do
      expect(visit.webpage).to eq "/index"
    end
  end

  describe "#ip_address" do
    it "returns visit\'s ip_address" do
      expect(visit.ip_address).to eq "444.701.448.104"
    end
  end

end
