require "sp_log_parser"

RSpec.describe Visit do
  let(:logs) { File.readlines("spec/support/webserver.log") }
  let(:visits) { Parser.parse(logs) }
  let(:stats) { Stats.new(visits) }

  context "displaying views" do
    it "returns corect output" do
      expected = "/help_page/1 => 5 views\n/contact => 2 views\n"

      expect { StatsDisplay.display(stats.views.first(2)) }.to(
        output(expected).to_stdout
      )
    end
  end

  context "displaying unique views" do
    it "returns corect output" do
      expected = "/help_page/1 => 5 unique views\n/contact => 1 unique view\n"

      expect { StatsDisplay.display(stats.unique_views.first(2), unique: true) }.to(
        output(expected).to_stdout
      )
    end
  end
end
